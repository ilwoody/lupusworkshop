//
//  main.m
//  LupusTest
//
//  Created by Nicola Ferruzzi on 21/05/14.
//  Copyright (c) 2014 Nicola Ferruzzi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
