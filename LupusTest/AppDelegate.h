//
//  AppDelegate.h
//  LupusTest
//
//  Created by Nicola Ferruzzi on 21/05/14.
//  Copyright (c) 2014 Nicola Ferruzzi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
