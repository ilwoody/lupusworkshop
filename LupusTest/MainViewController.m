//
//  MainViewController.m
//  LupusTest
//
//  Created by Nicola Ferruzzi on 21/05/14.
//  Copyright (c) 2014 Nicola Ferruzzi. All rights reserved.
//

#import "MainViewController.h"
#import "GameViewController.h"
#import "Lupus.h"

@interface MainViewController () <MCBrowserViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *button_create;
@property (weak, nonatomic) IBOutlet UIButton *button_search;
@property (strong, nonatomic) LupusGame *game;
@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.button_search addTarget:self
                           action:@selector(onSearch:)
                 forControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)onCreate:(id)sender
{
    self.game = [LupusGame lupusGameWithHostName:@"Nicola game" options:nil];
    [self performSegueWithIdentifier:@"segue_game" sender:nil];
}

- (IBAction)onSearch:(id)sender
{
    self.game = [LupusGame lupusGameWithPlayerName:@"Nicola player"];
    MCBrowserViewController *vc = self.game.browser;
    vc.delegate = self;
    [self.navigationController setNavigationBarHidden:TRUE];
    [self.navigationController pushViewController:vc animated:true];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_game"]) {
        GameViewController *vc = segue.destinationViewController;
        vc.game = self.game;
        [self.navigationController setNavigationBarHidden:FALSE];
    }
}

- (void)browserViewControllerDidFinish:(MCBrowserViewController *)browserViewController
{
    NSLog(@"finish");
    self.navigationController.viewControllers = @[self];
    [self performSegueWithIdentifier:@"segue_game" sender:nil];
}

- (void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController
{
    NSLog(@"cancelled");
    [self.navigationController popViewControllerAnimated:TRUE];
    [self.game disconnect];
}

@end
